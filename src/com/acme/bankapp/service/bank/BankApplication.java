package com.acme.bankapp.service.bank;


import com.acme.bankapp.domain.bank.Account;
import com.acme.bankapp.domain.bank.Bank;
import com.acme.bankapp.domain.bank.CheckingAccount;
import com.acme.bankapp.domain.bank.Client;
import com.acme.bankapp.domain.bank.Gender;
import com.acme.bankapp.domain.bank.SavingAccount;

public class BankApplication {
	private static int OVERDRAFT_MAX_AMOUNT = 5000;
	private static int DEPOSIT_MAX_AMOUNT = 1000;
	
	private static void fillBank(Bank bank) {
		Client client;
		Account account;
		String[] names = {"Smith", "Black", "White", "Jhones", "Park", "Simpson", "Bale", "Ronaldo", "Carrick", "Giggs", "Scholes", "De Hea"};
		for (int i = 0; i < 10; i++) {
			client = new Client(Gender.values()[(int)Math.rint(Math.random())], names[i]);
			if (Math.random() < 0.5d) {
				account = new SavingAccount();
			} else {
				CheckingAccount checkAccount = new CheckingAccount();
				checkAccount.setOverdraft(Math.random() * OVERDRAFT_MAX_AMOUNT);
				account = checkAccount;
			}
			account.deposit(Math.random() * DEPOSIT_MAX_AMOUNT);
			client.getAccounts().add(account);
			BankService.addClient(client, bank);
		}
	}
	
	private static void modifyBank(Bank bank) {
		int n = (int) Math.rint(Math.random() * 9);
		Client client = bank.getClients().get(n);
		double amount = client.getAccounts().get(0).getBalance();
		if(Math.random() > 0.5d) {
			client.getAccounts().get(0).deposit(Math.random() * amount);
		} else {
			client.getAccounts().get(0).withdraw(Math.random() * amount);
		} 
	}

	public static void main(String[] args) {
		Bank bank = new Bank();
		fillBank(bank);
		System.out.println(BankService.printAccounts(bank));
		modifyBank(bank);
		System.out.println(BankService.printAccounts(bank));
		System.out.println(BankService.printMaximumAmountToWithdraw(bank));
	}

}
