package com.acme.bankapp.service.bank;

import com.acme.bankapp.domain.bank.Account;
import com.acme.bankapp.domain.bank.Bank;
import com.acme.bankapp.domain.bank.Client;

public class BankService {
	public static void addClient(Client client, Bank bank) {
		bank.addClient(client);
	}

	public static String printMaximumAmountToWithdraw(Bank bank) {
		StringBuilder result = new StringBuilder();
		for (Client client: bank.getClients()) {
			result.append('\n');
			result.append("Client: " + client.getClientSalutation() + ' ');
			for (Account acc : client.getAccounts()) {
				result.append(acc.maximumAmountToWithdraw() + ' ');
			}
		}
		return result.toString();
	}
	
	public static String printAccounts(Bank bank) {
		StringBuilder result = new StringBuilder();
		for (Client client: bank.getClients()) {
			result.append('\n');
			result.append("Client: " + client.getClientSalutation() + ' ');
			for (Account acc : client.getAccounts()) {
				result.append(acc.getBalance() + ' ');
			}
		}
		return result.toString();
	}
}
