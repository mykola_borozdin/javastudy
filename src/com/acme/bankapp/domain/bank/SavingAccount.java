package com.acme.bankapp.domain.bank;

public class SavingAccount extends AbstractAccount {
	public double maximumAmountToWithdraw() {
		return getBalance();
	}
}
