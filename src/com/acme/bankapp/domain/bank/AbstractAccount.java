package com.acme.bankapp.domain.bank;

public abstract class AbstractAccount implements Account{
	private double balance;
	public double getBalance() {
		return balance;
	}
	public void deposit(double amount) {
		if (amount >= 0.0) {
			balance += amount; 
		}
	}
	public void withdraw(double amount) {
		if (amount >= 0.0 && amount <= balance) {
			balance -= amount;
		}
	}
	protected void setBalance(double newBalance) {
		balance = newBalance;
	}
}
