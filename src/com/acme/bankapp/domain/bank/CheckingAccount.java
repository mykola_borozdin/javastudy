package com.acme.bankapp.domain.bank;

public class CheckingAccount extends AbstractAccount {
	private double overdraft;
	
	@Override
	public void withdraw(double amount) {
		if (amount >= 0.0 && amount <= getBalance() + getOverdraft()) {
			if (amount <= getBalance()) {
				this.setBalance(getBalance() - amount);
				assert this.getBalance() >= 0.0d: "Balance is " + this.getBalance();
			} else {
				amount -= getBalance();
				this.setBalance(0.0d);
				overdraft -= amount;
				assert this.getBalance() >= 0.0d: "Balance is " + this.getBalance();
				assert overdraft >= 0.0d: "Overdraft is " + overdraft;
			}
		} else {
			if (amount < 0) {
				throw new IllegalArgumentException("Amount < 0");
			} else {
				throw new IllegalArgumentException("Insufficient funds");
			}				
		}
	}
	
	public double maximumAmountToWithdraw() {
		return getBalance() + overdraft;
	}

	public double getOverdraft() {
		return overdraft;
	}

	public void setOverdraft(double overdraft) {
		this.overdraft = overdraft;
	}
}
