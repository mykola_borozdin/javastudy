package com.acme.bankapp.domain.bank;

public enum Gender {
	MALE("Mr."), FEMALE("Ms.");
	private String greeting;
	Gender(String str) {
		greeting = str;
	}
	String getGreeting() {
		return greeting;
	}
}
