package com.acme.bankapp.domain.bank;

import java.util.ArrayList;

public class Client {
	private ArrayList<Account> accounts;
	private Gender gender;
	private String name;
	
	public Client(Gender gender, String name) {
		accounts = new ArrayList<Account>();
		this.gender = gender;
		this.name = name;
	}

	public ArrayList<Account> getAccounts() {
		return accounts;
	}

	public void setAccounts(ArrayList<Account> accounts) {
		this.accounts = accounts;
	}
	
	public String getClientSalutation() {
		return gender.getGreeting() + name;
	}

}
