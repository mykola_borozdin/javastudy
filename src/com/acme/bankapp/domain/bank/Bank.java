package com.acme.bankapp.domain.bank;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;


public class Bank extends ClientRegistrationTalker {
	private ArrayList<Client> clients;

	public Bank() {
		clients = new ArrayList<Client>();
		this.registerObserver(new DebugListener());
		this.registerObserver(new EmailNotificationListner());
		this.registerObserver(new PrintClientListener());
	}

	public boolean addClient(Client client) {
		boolean result = clients.add(client);
		if (result) {
			this.notifyObservers(client);
		}
		return result;
	}

	public List<Client> getClients() {
		return clients;
	}

	private class DebugListener implements ClientRegistrationListener {
		
		@Override
		public void onClientAdded(Client c) {
			System.out.println(c.getClientSalutation() + " at "
					+ new Date(System.currentTimeMillis()));
		}
	}

	private class EmailNotificationListner implements ClientRegistrationListener {

		@Override
		public void onClientAdded(Client c) {
			System.out.println("Notification email for client "
					+ c.getClientSalutation() + " to be sent");

		}
	}

	private class PrintClientListener implements ClientRegistrationListener {

		@Override
		public void onClientAdded(Client c) {
			System.out.println(c.getClientSalutation());
		}
	}
}

interface ClientRegistrationListener {
	
	void onClientAdded(Client c);
}

abstract class ClientRegistrationTalker {
	
	private HashSet<ClientRegistrationListener> listeners;

	public ClientRegistrationTalker() {
		listeners = new HashSet<ClientRegistrationListener>();
	}

	public boolean registerObserver(ClientRegistrationListener listener) {
		if (listener != null) {
			return listeners.add(listener);
		} else {
			return false;
		}
	}

	public boolean unRegisterObserver(ClientRegistrationListener listener) {
		if (listener != null) {
			return listeners.remove(listener);
		} else {
			return false;
		}
	}

	protected void notifyObservers(Client c) {
		for (ClientRegistrationListener listener : listeners) {
			listener.onClientAdded(c);
		}
	}
}
