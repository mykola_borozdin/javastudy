package com.acme.bankapp.util;

public abstract class Observer {
	public abstract void actionPerformed();
}
