package com.acme.bankapp.util;

import java.util.HashSet;

public abstract class Observable {
	private HashSet<Observer> observers;
	
	public Observable() {
		observers = new HashSet<Observer>();
	}
	
	public boolean registerObserver(Observer observer) {
		if (observer != null) {
			return observers.add(observer);
		} else {
			return false;
		}
	}
	
	public boolean unRegisterObserver(Observer observer) {
		if (observer != null) {
			return observers.remove(observer);
		} else {
			return false;
		}
	}
	
	protected void notifyObservers() {
		for (Observer observer: observers) {
			observer.actionPerformed();
		}
	}
}
